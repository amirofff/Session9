package amirofff.test.session4;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class TypeFaceActivity extends AppCompatActivity {
    TextView text1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_type_face);

        text1 = (TextView) findViewById(R.id.text1);
        Typeface andlso = Typeface.createFromAsset(getAssets() , "andlso.ttf");
        text1.setTypeface(andlso);
    }
}
