package amirofff.test.session4.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import amirofff.test.session4.R;

/**
 * Created by macbook on 7/3/2017 AD.
 */

public class FragmentA extends Fragment {

    public static FragmentA fragment;
    public static FragmentA getInstance(){
        if(null==fragment)
            fragment=new FragmentA();
        return fragment;
    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View myView = inflater.inflate(R.layout.fragment_a, container, false);
//        TextView myText = (TextView) myView.findViewById(R.id.myText);
//        myText.setText("Amir Farahani");
        return myView;
    }
}
