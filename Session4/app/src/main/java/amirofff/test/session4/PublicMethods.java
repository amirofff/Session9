package amirofff.test.session4;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by macbook on 6/13/2017 AD.
 */

public class PublicMethods {
    
    Context mContext;

    public PublicMethods(Context mContext) {
        this.mContext = mContext;
    }
    
    public void  showToast(String message) {
        Toast.makeText(mContext, "", Toast.LENGTH_SHORT).show();
        
    }
}
