package amirofff.test.session4;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by macbook on 6/23/2017 AD.
 */

public class IncomingCallReceiver extends BroadcastReceiver{


    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Incomming Call", Toast.LENGTH_LONG).show();
    }
}
