package amirofff.test.session4.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import amirofff.test.session4.fragments.FragmentA;
import amirofff.test.session4.fragments.FragmentB;
import amirofff.test.session4.fragments.FragmentC;
import amirofff.test.session4.fragments.FragmentD;

/**
 * Created by macbook on 7/3/2017 AD.
 */

public class MyPagerAdapter extends FragmentPagerAdapter {
    public MyPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        if(position==0)
            return FragmentA.getInstance();
        if(position==1)
            return FragmentB.getInstance();
        if(position==2)
            return FragmentC.getInstance();
        if(position==3)
            return FragmentD.getInstance();

        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }

    @Override
    public CharSequence getPageTitle(int position) {

        if(position==0)
            return "Frag A";
        if(position==1)
            return "Frag B";
        if(position==2)
            return "Frag C";
        if(position==3)
            return "Frag D";

        return null;
    }
}
