package amirofff.test.session4;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class SplashScreenActivity extends AppCompatActivity {

    Boolean backPressed = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent menuIntent = new Intent(SplashScreenActivity.this ,  MenuActivity.class);
                startActivity(menuIntent);
            }
        },3000);



    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
//        Toast.makeText(this, "Don't Back", Toast.LENGTH_SHORT).show();
            if (backPressed){
                finish();
            }else {
                backPressed = true;
                Toast.makeText(this, "press back again for exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        backPressed=false;
                    }
                },3000);
            }
    }
}
