package amirofff.test.session4;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.astuetz.PagerSlidingTabStrip;

import amirofff.test.session4.adapters.MyPagerAdapter;

public class PagerActivity extends AppCompatActivity {

    ViewPager myPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);

        myPager = (ViewPager) findViewById(R.id.myPager);
        MyPagerAdapter adapter = new MyPagerAdapter(getSupportFragmentManager());
        myPager.setAdapter(adapter);



        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(myPager);

    }
}
